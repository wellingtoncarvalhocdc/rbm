"use strict";
function sum(num1, num2) {
    return num1 + num2;
}
console.log(sum(1, 2));
function average(array) {
    const arr_length = array.length;
    const sum = array.reduce((acc, cur) => acc + cur, 0);
    return sum / arr_length;
}
console.log(average([1, 2, 3, 4, 5]));
function IMC(weight, height) {
    return +(weight / height ** 2).toFixed(2);
}
console.log(IMC(80, 1.75));
